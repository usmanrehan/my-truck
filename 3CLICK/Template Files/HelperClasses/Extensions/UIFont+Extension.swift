
import Foundation
import UIKit

extension UIFont {
    func sizeOfString(text: NSString, constrainedToWidth width: Double) -> CGSize {
        return text.boundingRect(with: CGSize(width: width, height: .greatestFiniteMagnitude),
                                 options: .usesLineFragmentOrigin,
                                 attributes: [NSAttributedString.Key.font: self],
                                 context: nil).size
    }
}
