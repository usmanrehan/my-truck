
import Foundation

enum Strings: String{
    //MARK:- Notifies
    case ALERT = "Alert"
    case ERROR = "Error"
    case UNKNOWN_ERROR = "Unknown error"
    case YES = "Yes"
    case NO = "No"
    case OK = "Ok"
    case SUCCESS = "Success"
    case Confirmation = "Confirmation"
    case PIN_SENT_EMAIL = "A verification PIN has been sent to your provided email address."
    case EDIT = "Edit"
    case SAVE = "Save"
    case CANCEL = "Cancel"
    case VERIFIED = "Verified"
    
    //MARK:- Validation
    case EMPTY_LOGIN_FIELDS = "Please provide your valid email address or mobile number to login."
    case PROFILE_IMAGE_REQUIRED = "Please upload your profile picture."
    case INVALID_NAME = "Name should contain atleast 3 characters."
    case INVALID_EMAIL = "Please provide a valid Email Address."
    case INVALID_PHONE = "Please provide a valid Phone Number."
    case INVALID_OTP = "Please provide a valid OTP Code."
    case INVALID_COUNTRY = "Please select your country."
    case EMPTY_PWD = "Please provide password."
    case EMPTY_CONFIRM_PWD = "Please confirm your password."
    case INVALID_PWD = "Password should contain atleast 6 characters."
    case PWD_ATLEAST_SIX_CH = "At least 6 characters."
    case PWD_DONT_MATCH = "New password and confirm password does not match."
    case ALL_FIELD_REQ = "All Fields are required!"
    case INVALID_PIN = "Please provide complete PIN."
    case PWD_CHANGED = "You have successfully updated your password."
    case PLEASE_AGREE_TO_TNC = "Please agree to our terms and conditions."
    case URL_NOT_VALID = "Invalid URL"
    case LOGOUT = "Logout"
    case ASK_LOGOUT = "Are you sure you want to logout?"

    case SELECT_LOCATION = "Please select your pick-up/drop-off locations."
    case SELECT_DATE = "Please select shipment date."
    case SELECT_COMMODITY = "Please select commodity."
    case SELECT_TRUCK_TYPE = "Please select truck type."
    case SELECT_TRUCK_SIZE = "Please select truck size."
    case SELECT_WEIGHT_RANGE = "Please select weight range."
    case INVALID_WEIGHT_SELECTED = "Please select valid weight of shipment."
    case SHIPMENT_ASSIGNED = "Shipment assigned successfully"
    case PASSWORD_CHANGED = "Password changed successfully\n Please login to continue using Truck-Online."
    case INVALID_CARD_NUMBER = "Your card's number is invalid"
    
    case ERROR_GENERIC_MESSAGE = "Unable to connect server\n Please check your internet connection and try again later."
    case TOKEN_EXPIRED = "Invalid Authentication Token Supplied."
    case PASSWORD_UPDATED = "Password updated successfully."
    case PROFILE_UPDATED = "Profile updated successfully."
    case NO_RESULTS = "No Results"
    case NO_NOTIFICATIONS = "No notifications available."
    case NO_NOTIFICATIONS_DESC = "When you have notification(s), you'll see them here."
    case NO_CARDS = "No card(s) available."
    case NO_CARDS_DESC = "When you have card(s), you'll see them here."
    case NO_RIDES = "No bookings available."
    case NO_RIDESS_DESC = "When you have booking(s), you'll see them here."
    case NO_HELP = "No help available."
    case NO_HELP_DESC = "When you have help guide, you'll see them here."
    case REPORT = "Report"
    case REPORT_DESC = "Are you sure you want to report this ride?"
    case CANCEL_REASON = "Please enter your reason to cancel this booking."
    
    //MARK:- Error messages
    case RESPONSE_ERROR = "Invalid response for route:"
    
    //SingleMethod For the Usage of Localization.
    var text: String { return NSLocalizedString( self.rawValue, comment: "") }
    
    //App level strings
    case HIRE_FLEET = "Hire fleet"
    case SELECT_PAYMENT_METHOD = "Please select payment method"
    
    //MARK:- Controllers title
    case HOME = "Select Location"
    case YOUR_RIDES = "Your Rides"
    case BOOKINGS = "Bookings"
    case CURRENT_BOOKINGS = "In-transit Trip"
    case PENDING_BOOKINGS = "Pending Trip"
    case PREVIOUS_BOOKINGS = "Completed Trip"
    case ABOUT_US = "About Us"
    case NOTIFICATIONS = "Notifications"
    case HELP = "Help"
    case SETTINGS = "Settings"
    case FLEETS = "Fleets"
    case FLEET_DETAILS = "Fleet Details"
    case PROFILE = "Profile"
    case TRACK_RIDE = "Track Ride"
    case ALL_CARDS = "All Cards"
    case TRANSACTIONS = "Transactions"
    
    //MARK:- Settings options
    case UPDATE_PROFILE = "Update Profile"
    case NAME = "Name"
    case MOBILE = "Mobile"
    case EMAIL = "Email"
    case CHANGE_PASSWORD = "Change Password"
    case OLD_PASSWORD = "Old password"
    case NEW_PASSWORD = "New password"
    case CONFIRM_PASSWORD = "Confirm password"
    case ADD_CARD_DETAILS = "Add Card Details"
    
    //APIs Messages
    case signUpSuccess = "You've successfully signup to Transmissito\nPlease login now."
    
    //MARK:- RIDES STATUS
    case APPROVED   = "Approved"
    case INPROGRESS = "In Progress"
    case ARRIVED    = "Arrived"
    //case PENDING    = "Pending"
    case COMPLETED  = "Completed"
    case CANCELLED     = "Cancelled"
    
    case RIDE_ENDED         = "Ride ended"
    case RIDE_ENDED_MESSAGE = "Your ride has ended"
    
    case DELETE_CARD = "Are you sure! you want to remove card details?"
    case CARD_ADDED = "Card details added successfully."
    case CARD_DELETED = "Card details deleted successfully."
    case DELETE_NOTIFICATIONS = "Are you sure you want to delete all the notifications?"
    case HIRE_NOW_LATER = "Do you want to hire now or later?"
    case NOW = "Now"
    case LATER = "Later On"
    
    case INVALID_EMAIL_CODE = "Please enter complete verification code from email we sent you."
    case INVALID_PHONE_CODE = "Please enter complete verification code from phone number we sent you."
    case INVALID_VERIFICATION_CODE = "Please enter complete verification code from email/phone number we sent you."
    
    case RESENT = "Code Resent"
    case RESENT_MESSAGE = "Code has been sent to your provided email address."
    
    case INVALID_PROVIDED_NUMBER = "Provided number is not valid."
    
    case NEW_CARD_ADDED = "New card added successfully."
    case DELETE = "Delete"
    case ACTIVE = "Active"
    case IN_ACTIVE = "In Active"
    case ALL_INACTIVE_CARDS = "Please activate atleats one card to continue hire bookings.\nSwipe left to update card's status"
    
    case TRUST_TRANSMISSITO = "Trust Transmissito"
    case BROADCAST_THIS_RIDE = "Are you sure you want to broadcast this ride?"
    case BROADCASTED_RIDE = "Your booking is in process.\nwe will get back to you in a while."
    
    case FORGOT_PASSWORD = "Forgot Password"
    case EMAIL_OTP = "Email OTP"
    case ENTER_EMAIL_PIN = "Enter your email pin and new password to"
    
    case CURRENCY = "Rs"
    
    case PAY_NOW = "Pay now"
    case PAY_BY_WALLET = "Pay by wallet"
}
