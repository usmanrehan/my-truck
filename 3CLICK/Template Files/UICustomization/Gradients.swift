
import Foundation
import UIKit

class OrangeGradient : UIButton{
    override func layoutSubviews() {
        super.layoutSubviews()
        let darkBlue = UIColor(displayP3Red: 252/255, green: 44/255, blue: 58/255, alpha: 1.0)
        let lightBlue = UIColor(red: 249/255, green: 104/255, blue: 22/255, alpha: 1.0)
        self.applyGradient(colours: [darkBlue,lightBlue])
    }
    @IBInspectable var iPhoneFontSize:CGFloat = 0 {
        didSet {
            overrideFontSize(fontSize: iPhoneFontSize)
        }
    }
    func overrideFontSize(fontSize:CGFloat){
        let currentFontName = self.titleLabel?.font.fontName ?? "system"
        var calculatedFont: UIFont?
        let bounds = UIScreen.main.bounds
        let height = bounds.size.height
        switch height {
        case 480.0: //Iphone 3,4,SE => 3.5 inch
            calculatedFont = UIFont(name: currentFontName, size: fontSize * 0.7)
            self.titleLabel?.font = calculatedFont
            break
        case 568.0: //iphone 5, 5s => 4 inch
            calculatedFont = UIFont(name: currentFontName, size: fontSize * 0.8)
            self.titleLabel?.font = calculatedFont
            break
        case 667.0: //iphone 6, 6s => 4.7 inch
            calculatedFont = UIFont(name: currentFontName, size: fontSize * 0.9)
            self.titleLabel?.font = calculatedFont
            break
        case 736.0: //iphone 6s+ 6+ => 5.5 inch
            calculatedFont = UIFont(name: currentFontName, size: fontSize)
            self.titleLabel?.font = calculatedFont
            break
        default:
            print("not an iPhone")
            break
        }
    }
}
class BlueGradient : UIButton{
    override func layoutSubviews() {
        super.layoutSubviews()
        let darkBlue = UIColor(displayP3Red: 5/255, green: 81/255, blue: 193/255, alpha: 1.0)
        let lightBlue = UIColor(red: 3/255, green: 117/255, blue: 206/255, alpha: 1.0)
        self.applyGradient(colours: [darkBlue,lightBlue])
    }
    @IBInspectable var iPhoneFontSize:CGFloat = 0 {
        didSet {
            overrideFontSize(fontSize: iPhoneFontSize)
        }
    }
    func overrideFontSize(fontSize:CGFloat){
        let currentFontName = self.titleLabel?.font.fontName ?? "system"
        var calculatedFont: UIFont?
        let bounds = UIScreen.main.bounds
        let height = bounds.size.height
        switch height {
        case 480.0: //Iphone 3,4,SE => 3.5 inch
            calculatedFont = UIFont(name: currentFontName, size: fontSize * 0.7)
            self.titleLabel?.font = calculatedFont
            break
        case 568.0: //iphone 5, 5s => 4 inch
            calculatedFont = UIFont(name: currentFontName, size: fontSize * 0.8)
            self.titleLabel?.font = calculatedFont
            break
        case 667.0: //iphone 6, 6s => 4.7 inch
            calculatedFont = UIFont(name: currentFontName, size: fontSize * 0.9)
            self.titleLabel?.font = calculatedFont
            break
        case 736.0: //iphone 6s+ 6+ => 5.5 inch
            calculatedFont = UIFont(name: currentFontName, size: fontSize)
            self.titleLabel?.font = calculatedFont
            break
        default:
            print("not an iPhone")
            break
        }
    }
}

extension UIView {
    func applyGradient(colours: [UIColor]) -> Void {
        self.applyGradient(colours: colours, locations: nil)
    }
    func applyGradient(colours: [UIColor], locations: [NSNumber]?) -> Void {
        let gradient: CAGradientLayer = CAGradientLayer()
        gradient.frame = self.bounds
        gradient.colors = colours.map { $0.cgColor }
        gradient.startPoint = CGPoint(x: 0.0, y: 0.5)
        gradient.endPoint = CGPoint(x:1.0, y: 0.5)
        self.layer.insertSublayer(gradient, at: 0)
    }
}
