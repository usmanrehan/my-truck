//
//  RateRide.swift
//  3CLICK
//
//  Created by Sierra-PC on 24/09/2019.
//  Copyright © 2019 Sierra-PC. All rights reserved.
//

import UIKit

class RateRide: UIViewController {
    
    @IBOutlet weak var viewRating: CosmosView!
    var rideDetail: RideDetailModel?

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    @IBAction func onBtnDismiss(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    @IBAction func onBtnSubmit(_ sender: UIButton) {
        self.addRating()
    }
}
extension RateRide{
    private func addRating(){

        let booking = self.rideDetail?.id ?? "0"
        let rating = "\(self.viewRating.rating)"

        let params:[String:Any] = ["booking":booking,
                                   "rating":rating]
        APIManager.sharedInstance.usersAPIManager.AddRating(params: params, success: { (responseObject) in
            print(responseObject)
            self.dismiss(animated: true, completion: nil)
        }) { (error) in
            print(error)
        }
    }
}
