//
//  RiderNavigation.swift
//  3CLICK
//
//  Created by Sierra-PC on 01/08/2019.
//  Copyright © 2019 Sierra-PC. All rights reserved.
//

import UIKit
import GoogleMaps
import Alamofire
import SwiftyJSON
import SocketIO

class RiderNavigation: BaseController {

    @IBOutlet weak var mapView: GMSMapView!
    var rideDetail = RideDetailModel()
    
    var polyline = GMSPolyline()
    var animationPolyline = GMSPolyline()
    var path = GMSPath()
    var animationPath = GMSMutablePath()
    var i: UInt = 0
    var timer: Timer!
    
    //var driverLocation: CLLocationCoordinate2D?
    var pickUpLocation : CLLocationCoordinate2D?
    var dropOffLocation: CLLocationCoordinate2D?
    
    var driverLocationMarker = GMSMarker()
    var pickUpLocationMarker = GMSMarker()
    var dropOffLocationMarker = GMSMarker()
    
    var isDriverPlaced = false
    var canFitToBounds = true
    var timerUpdateDriver: Timer!
    var timerDispatcherSourceTimer: DispatchSourceTimer?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = Strings.TRACK_RIDE.text
        self.mapView.delegate = self
        //self.getRiderDeviceToken()
        self.startTimer()
        // Do any additional setup after loading the view.
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.setMarkers()
        DispatchQueue.main.asyncAfter(deadline: .now() + 2.0) {
            self.connectSocket()
        }
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.stopTimer()
        self.disconnectSocket()
        Constants.driverLocation = nil
        self.mapView.delegate = nil
        self.mapView.clear()
        self.mapView.removeFromSuperview()
        self.mapView = nil
    }
    
}
extension RiderNavigation{
    private func setMarkers(){
//        guard let pickUpLat = CLLocationDegrees(self.rideDetail.pickUpLatitude ?? "0.0") else {return}
//        guard let pickUpLng = CLLocationDegrees(self.rideDetail.pickUpLongitude ?? "0.0") else {return}
//        self.pickUpLocation = CLLocationCoordinate2D(latitude: pickUpLat, longitude: pickUpLng)
        
        guard let dropOffLat = CLLocationDegrees(self.rideDetail.dropOffLatitude ?? "0.0") else {return}
        guard let dropOffLng = CLLocationDegrees(self.rideDetail.dropOffLongitude ?? "0.0") else {return}
        self.dropOffLocation = CLLocationCoordinate2D(latitude: dropOffLat, longitude: dropOffLng)
        
        //self.addPickUpMarker()
        self.addDropOffMarker()
        //self.fitAllMarkersBounds()
    }
//    private func fitAllMarkersBounds() {
//        var bounds = GMSCoordinateBounds()
//        let markerList = [self.driverLocationMarker,self.dropOffLocationMarker]
//        for marker in markerList {
//            bounds = bounds.includingCoordinate(marker.position)
//        }
//        let update = GMSCameraUpdate.fit(bounds, withPadding: CGFloat(50))
//        self.mapView.animate(with: update)
//    }
    private func fitAllMarkersBounds() {
        var bounds = GMSCoordinateBounds()
        var markerList = [GMSMarker]()
        if let pathBounds = self.getPathBounds(){
            markerList = pathBounds
        }
        markerList.append(self.driverLocationMarker)
//        markerList.append(self.dropOffLocationMarker)
        for marker in markerList {
            bounds = bounds.includingCoordinate(marker.position)
        }
        let update = GMSCameraUpdate.fit(bounds, withPadding: CGFloat(50))
        self.mapView.animate(with: update)
    }
    private func getPathBounds()->[GMSMarker]?{
        if self.path.count() == 0{return nil}
        var arrPinPoints = [GMSMarker]()
        let pathTaken = self.path.count()
        
        if pathTaken > 0{
            for i in 0..<pathTaken{
                let point = path.coordinate(at: i)
                let position = CLLocationCoordinate2D(latitude: point.latitude , longitude: point.longitude)
                arrPinPoints.append(GMSMarker(position: position))
            }
        }
        return arrPinPoints
    }
}
//MARK:- Polyline
extension RiderNavigation{
    private func getRoute(){
        let origin = "\(Constants.driverLocation?.latitude ?? 0.0),\(Constants.driverLocation?.longitude ?? 0.0)"
        let destination = "\(self.rideDetail.dropOffLatitude ?? "0.0"),\(self.rideDetail.dropOffLongitude ?? "0.0")"
        let url = "https://maps.googleapis.com/maps/api/directions/json?origin=\(origin)&destination=\(destination)&mode=driving&key=\(Constants.apiKey)"
        Alamofire.request(url).responseJSON { response in
            do {
                let json = try JSON(data: response.data ?? Data())
                let routes = json["routes"].arrayValue
                self.drawRoute(routesArray: routes)
            } catch {
                print("Hm, something is wrong here. Try connecting to the wifi.")
            }
        }
    }
    private func drawRoute(routesArray: [JSON]) {
        self.resetPolyline()
        if !routesArray.isEmpty{
            let routeDict = routesArray[0]
            let routeOverviewPolyline = routeDict["overview_polyline"].dictionary
            let points = routeOverviewPolyline?["points"]?.stringValue
            self.path = GMSPath.init(fromEncodedPath: points ?? "")!
            self.polyline.path = path
            self.polyline.strokeColor = Global.APP_COLOR
            self.polyline.strokeWidth = 3.0
            self.polyline.map = self.mapView
            self.timer = Timer.scheduledTimer(timeInterval: 0.5, target: self, selector: #selector(animatePolylinePath), userInfo: nil, repeats: true)
        }
    }
    @objc private func animatePolylinePath() {
//        if (self.i < self.path.count()) {
//            self.animationPath.add(self.path.coordinate(at: self.i))
//            self.animationPolyline.path = self.animationPath
//            self.animationPolyline.strokeColor = UIColor.lightGray
//            self.animationPolyline.strokeWidth = 3
//            self.animationPolyline.map = self.mapView
//            self.i += 1
//        }
//        else {
//            self.resetPolyline()
//        }
    }
    private func resetPolyline(){
        self.i = 0
        self.animationPath = GMSMutablePath()
        self.animationPolyline.map = nil
    }
}
//MARK:- Add marker
extension RiderNavigation{
    private func addPickUpMarker(){
        var markerIcon: UIImageView?
        let pickUpPin = UIImage(named: "location_p")!.withRenderingMode(.alwaysOriginal)
        let markerView = UIImageView(image: pickUpPin)
        markerIcon = markerView
        markerIcon?.frame = CGRect(x: 0, y: 0, width: 40, height: 50)
        self.pickUpLocationMarker.map = nil
        self.pickUpLocationMarker = GMSMarker(position: self.pickUpLocation ?? CLLocationCoordinate2D())
        self.pickUpLocationMarker.iconView = markerIcon
        self.pickUpLocationMarker.tracksViewChanges = true
        self.pickUpLocationMarker.map = self.mapView
    }
    private func addDropOffMarker(){
        var markerIcon: UIImageView?
        let dropOffPin = UIImage(named: "location_d")!.withRenderingMode(.alwaysOriginal)
        let markerView = UIImageView(image: dropOffPin)
        markerIcon = markerView
        markerIcon?.frame = CGRect(x: 0, y: 0, width: 40, height: 50)
        self.dropOffLocationMarker.map = nil
        self.dropOffLocationMarker = GMSMarker(position: self.dropOffLocation ?? CLLocationCoordinate2D())
        self.dropOffLocationMarker.iconView = markerIcon
        self.dropOffLocationMarker.tracksViewChanges = true
        self.dropOffLocationMarker.map = self.mapView
        self.zoomToSearchLocation(location: self.dropOffLocation ?? CLLocationCoordinate2D())
    }
    private func addDriverMarker(){
        if !self.isDriverPlaced{
            var markerIcon: UIImageView?
            let pickUpPin = UIImage(named: "location1")!.withRenderingMode(.alwaysOriginal)
            let markerView = UIImageView(image: pickUpPin)
            markerView.contentMode = .scaleAspectFit
            markerIcon = markerView
            markerIcon?.frame = CGRect(x: 0, y: 0, width: 50, height: 60)
            //self.driverLocationMarker.map = nil
            self.driverLocationMarker = GMSMarker(position: Constants.driverLocation ?? CLLocationCoordinate2D())
            self.driverLocationMarker.iconView = markerIcon
            self.driverLocationMarker.tracksViewChanges = false
            self.driverLocationMarker.map = self.mapView
            self.isDriverPlaced = true
        }
        else{
            CATransaction.begin()
            CATransaction.setAnimationDuration(1.0)
            self.driverLocationMarker.position =  Constants.driverLocation ?? CLLocationCoordinate2D()
            CATransaction.commit()
        }
    }
    private func zoomToSearchLocation(location: CLLocationCoordinate2D){
        CATransaction.begin()
        CATransaction.setValue(1, forKey: kCATransactionAnimationDuration)
        let fancy = GMSCameraPosition.camera(withLatitude: location.latitude,
                                             longitude: location.longitude,
                                             zoom: 6,
                                             bearing: 0,
                                             viewingAngle: 0)
        self.mapView.animate(to: fancy)
        CATransaction.commit()
    }
    
}
//MARK:- Handle Driver Marker
extension RiderNavigation{
    private func handleDriverMarker(){
        if Constants.driverLocation == nil{return}
        self.addDriverMarker()
//        if self.canFitToBounds{
//            self.fitAllMarkersBounds()
//            self.canFitToBounds = false
//        }
        self.fitAllMarkersBounds()
//        self.getRoute()
    }
    func startTimer() {
        if #available(iOS 10.0, *) {
            self.timerUpdateDriver = Timer.scheduledTimer(withTimeInterval: TimeInterval(5.0), repeats: true) { [weak self] _ in
                // do something here
                self?.handleDriverMarker()
            }
        }
    }
    func stopTimer() {
        self.timerUpdateDriver?.invalidate()
        //timerDispatchSourceTimer?.suspend() // if you want to suspend timer
        self.timerDispatcherSourceTimer?.cancel()
    }
}
//MARK:- GMSMapViewDelegate
extension RiderNavigation: GMSMapViewDelegate{
    func mapView(_ mapView: GMSMapView, idleAt position: GMSCameraPosition) {
        DispatchQueue.main.asyncAfter(deadline: .now() + 10, execute: {
            self.canFitToBounds = true
        })
    }
}
//MARK:- Service
extension RiderNavigation{
    private func getRiderDeviceToken(){
        let ride_id = self.rideDetail.id ?? ""
        let params:[String:Any] = ["ride_id":ride_id]
        APIManager.sharedInstance.usersAPIManager.RiderDeviceToken(params: params, success: { (responseObject) in
            let response = responseObject as Dictionary
            if let riderDeviceToken = response["device_token"] as? String{
                print(riderDeviceToken)
            }
        }) { (error) in
            print(error)
        }
    }
}
//MARK:- Socket
extension RiderNavigation{
    open class SocketConnection {
        public static let default_ = SocketConnection()
        let manager: SocketManager
        //private var socket: SocketIOClient
        private init() {
            //let token = AppStateManager.sharedInstance.loggedInUser.accessToken
            let param:[String:Any] = [:]//["token":token ?? ""]
            let route = "https://mto-socket.herokuapp.com/"
            let socketURL: URL = Utility.URLforRoute(route: route, params: param)! as URL
            manager = SocketManager(socketURL: socketURL, config: [.log(true), .compress])
            manager.config = SocketIOClientConfiguration(arrayLiteral: .connectParams(param), .secure(true))
        }
    }
    private func connectSocket(){
        let socket = SocketConnection.default_.manager.defaultSocket
        if socket.status != .connected{
            socket.connect()
        }
        socket.on(clientEvent: .connect) {data, ack in
            print("socket connected")
            self.getLatLng()
            self.getRideFinishedStatus()
        }
    }
    private func disconnectSocket(){
        let socket = SocketConnection.default_.manager.defaultSocket
        socket.disconnect()
    }
    private func getLatLng(){
        let socket = SocketConnection.default_.manager.defaultSocket
        let rideId = self.rideDetail.id ?? ""
        socket.on("receive-location-to-customer-\(rideId)") {data, ack in
            //print(data)
            guard let driverLocation = data.first as? NSDictionary else {return}
            guard let lat = driverLocation["lat"] as? String else {return}
            guard let lng = driverLocation["lng"] as? String else {return}
            let latitude  = Double(lat) ?? 0.0
            let longitude = Double(lng) ?? 0.0
            Constants.driverLocation = CLLocationCoordinate2D(latitude: latitude, longitude: longitude)
        }
    }
    private func getRideFinishedStatus(){
        let socket = SocketConnection.default_.manager.defaultSocket
        let rideId = self.rideDetail.id ?? ""
        socket.on("receive-ride-finish-to-customer-\(rideId)") {data, ack in
             print(data)
            socket.disconnect()
            Utility.main.showAlert(message: Strings.RIDE_ENDED_MESSAGE.text, title: Strings.RIDE_ENDED.text, controller: self, usingCompletionHandler: {
                self.navigationController?.popViewController(animated: true)
            })
        }
    }
}
