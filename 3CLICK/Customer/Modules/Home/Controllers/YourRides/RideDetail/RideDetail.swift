//
//  RideDetail.swift
//  3CLICK
//
//  Created by Sierra-PC on 28/05/2019.
//  Copyright © 2019 Sierra-PC. All rights reserved.
//

import UIKit
import ObjectMapper
import SDWebImage
import GoogleMaps
import SwiftyJSON
import Alamofire

class RideDetail: BaseController {
    
    @IBOutlet weak var mapView: GMSMapView!
    @IBOutlet weak var btnNavigate: OrangeGradient!
    @IBOutlet weak var lblPickUpAddress: UILabel!
    @IBOutlet weak var lblDropOffAddress: UILabel!
    @IBOutlet weak var lblTravelled: UILabel!
    @IBOutlet weak var lblBookingDate: UILabel!
    @IBOutlet weak var lblBookingTime: UILabel!
    @IBOutlet weak var lblWeightInTons: UILabel!
    @IBOutlet weak var lblAmountCharged: UILabel!
    @IBOutlet weak var viewAmountCharged: UIView!
    @IBOutlet weak var imgVendor: UIImageView!
    @IBOutlet weak var lblVendorName: UILabel!
    @IBOutlet weak var lblVendorEmail: UILabel!
    @IBOutlet weak var lblVendorPhone: UILabel!
    @IBOutlet weak var viewCancelBooking: UIView!
    
    var ride = Bookings()
    var rideDetail = RideDetailModel()
    var rideType = BookingType.current
    
    var polyline = GMSPolyline()
    var animationPolyline = GMSPolyline()
    var path = GMSPath()
    var animationPath = GMSMutablePath()
    var i: UInt = 0
    var timer: Timer!
    
    var pickUpLocation : CLLocationCoordinate2D?
    var dropOffLocation: CLLocationCoordinate2D?
    var pickUpLocationMarker = GMSMarker()
    var dropOffLocationMarker = GMSMarker()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setData()
        self.getRoute()
        self.mapView.delegate = self
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.getRideDetail()
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.setMarkers()
    }
    
    @IBAction func onBtnNavigate(_ sender: OrangeGradient) {
        super.pushToRiderNavigation(ride: self.rideDetail)
    }
    @IBAction func onBtnCall(_ sender: UIButton) {
        if let number = self.rideDetail.vendor?.contact, !number.isEmpty{
            Utility.main.makeCallTo(number: number)
        }
        else{
            Utility.main.showAlert(message: Strings.INVALID_PROVIDED_NUMBER.text, title: Strings.ERROR.text)
        }
    }
    @IBAction func onBtnCancelBooking(_ sender: UIButton) {
        super.presentCancelRide(rideDetail: self.rideDetail)
    }
    @IBAction func onBtnReportAProblem(_ sender: UIButton) {
        Utility.main.showAlert(message: Strings.REPORT_DESC.text, title: Strings.REPORT.text, controller: self) { (yes, no) in
            if yes != nil{
                self.reportAProblem()
            }
        }
    }
    
}
//MARK:- Helper Methods
extension RideDetail{
    private func setUI(){
        switch self.rideType{
        case .current:
            self.viewAmountCharged.isHidden = true
        case .pending:
            self.viewAmountCharged.isHidden = false
        case .finished:
            self.viewCancelBooking.isHidden = true
        case .inProgress:
            self.viewCancelBooking.isHidden = true
        case .cancelled:
            self.viewCancelBooking.isHidden = true
        }
        
        let data = self.rideDetail
        if (data.bookingStatus ?? "") == BookingType.inProgress.rawValue{
            self.btnNavigate.isHidden = false
        }
        else{
            self.btnNavigate.isHidden = true
        }
    }
    private func setData(){
        self.setUI()
        let data = self.rideDetail
        
        if (data.bookingStatus ?? "") == BookingType.finished.rawValue{
            if self.ride.rating == 0{
                super.presentRateRide(rideDetail: self.rideDetail)
            }
        }
        
        self.title = Utility.getFormattedDate(date: data.deliveryDate, format: "dd MMM yy, EEE h:mm a")
        let amount = "\(data.customerPayable)"
        
        let pickUpAddress = self.rideDetail.pickUpLocation ?? "Pick-up Location"
        let dropOffAddress = self.rideDetail.dropOffLocation ?? "Drop-off Location"
        
        self.lblPickUpAddress.text = pickUpAddress
        self.lblDropOffAddress.text = dropOffAddress
        
        self.lblBookingDate.text = Utility.getFormattedDate(date: data.deliveryDate, format: "dd MMM yy")
        self.lblBookingTime.text = Utility.getFormattedDate(date: data.deliveryDate, format: "EEE h:mm a")
        self.lblWeightInTons.text = data.weight ?? ""

        self.lblAmountCharged.text = "\(Strings.CURRENCY.text) \(amount)"
        
        if let imageURL = URL(string: data.vendor?.image ?? ""){
            self.imgVendor.sd_setImage(with: imageURL, completed: nil)
        }
        self.lblVendorName.text = data.vendor?.firstName ?? ""
        self.lblVendorEmail.text = data.vendor?.email ?? ""
        self.lblVendorPhone.text = data.vendor?.contact ?? ""
    }
    private func setMarkers(){
        guard let pickUpLat = CLLocationDegrees(self.rideDetail.pickUpLatitude ?? "0.0") else {return}
        guard let pickUpLng = CLLocationDegrees(self.rideDetail.pickUpLongitude ?? "0.0") else {return}
        self.pickUpLocation = CLLocationCoordinate2D(latitude: pickUpLat, longitude: pickUpLng)
        
        guard let dropOffLat = CLLocationDegrees(self.rideDetail.dropOffLatitude ?? "0.0") else {return}
        guard let dropOffLng = CLLocationDegrees(self.rideDetail.dropOffLongitude ?? "0.0") else {return}
        self.dropOffLocation = CLLocationCoordinate2D(latitude: dropOffLat, longitude: dropOffLng)
        
        self.addPickUpMarker()
        self.addDropOffMarker()
        self.fitAllMarkersBounds()
    }
    private func fitAllMarkersBounds() {
        var bounds = GMSCoordinateBounds()
        var markerList = [GMSMarker]()
        if let pathBounds = self.getPathBounds(){
            markerList = pathBounds
        }
        markerList.append(self.pickUpLocationMarker)
        markerList.append(self.dropOffLocationMarker)
        for marker in markerList {
            bounds = bounds.includingCoordinate(marker.position)
        }
        let update = GMSCameraUpdate.fit(bounds, withPadding: CGFloat(50))
        CATransaction.begin()
        CATransaction.setValue(1.0, forKey: kCATransactionAnimationDuration)
        self.mapView.animate(with: update)
        CATransaction.commit()
        
    }
    private func getPathBounds()->[GMSMarker]?{
        if self.path.count() == 0{return nil}
        var arrPinPoints = [GMSMarker]()
        let pathTaken = self.path.count()
        
        if pathTaken > 0{
            for i in 0..<pathTaken{
                let point = path.coordinate(at: i)
                let position = CLLocationCoordinate2D(latitude: point.latitude , longitude: point.longitude)
                arrPinPoints.append(GMSMarker(position: position))
            }
        }
        return arrPinPoints
    }
}
//MARK:- Helper Methods
extension RideDetail: GMSMapViewDelegate{
    func mapView(_ mapView: GMSMapView, didTap marker: GMSMarker) -> Bool {
        return true
    }
}
//MARK:- Polyline
extension RideDetail{
    private func getRoute(){
        let origin = "\(self.rideDetail.pickUpLatitude ?? "0.0"),\(self.rideDetail.pickUpLongitude ?? "0.0")"
        let destination = "\(self.rideDetail.dropOffLatitude ?? "0.0"),\(self.rideDetail.dropOffLongitude ?? "0.0")"
        let url = "https://maps.googleapis.com/maps/api/directions/json?origin=\(origin)&destination=\(destination)&mode=driving&key=\(Constants.apiKey)"
        Alamofire.request(url).responseJSON { response in
            do {
                let json = try JSON(data: response.data ?? Data())
                let routes = json["routes"].arrayValue
                self.drawRoute(routesArray: routes)
            } catch {
                print("Hm, something is wrong here. Try connecting to the wifi.")
            }
        }
    }
    private func drawRoute(routesArray: [JSON]) {
        self.resetPolyline()
        if !routesArray.isEmpty{
            let routeDict = routesArray[0]
            let routeOverviewPolyline = routeDict["overview_polyline"].dictionary
            let points = routeOverviewPolyline?["points"]?.stringValue
            self.path = GMSPath.init(fromEncodedPath: points ?? "")!
            self.polyline.path = path
            self.polyline.strokeColor = Global.APP_COLOR
            self.polyline.strokeWidth = 3.0
            self.polyline.map = self.mapView
            self.timer = Timer.scheduledTimer(timeInterval: 0.5, target: self, selector: #selector(animatePolylinePath), userInfo: nil, repeats: true)
            
            self.fitAllMarkersBounds()
        }
    }
    @objc private func animatePolylinePath() {
        //        if (self.i < self.path.count()) {
        //            self.animationPath.add(self.path.coordinate(at: self.i))
        //            self.animationPolyline.path = self.animationPath
        //            self.animationPolyline.strokeColor = UIColor.lightGray
        //            self.animationPolyline.strokeWidth = 3
        //            self.animationPolyline.map = self.mapView
        //            self.i += 1
        //        }
        //        else {
        //            self.resetPolyline()
        //        }
    }
    private func resetPolyline(){
        self.i = 0
        self.animationPath = GMSMutablePath()
        self.animationPolyline.map = nil
    }
}
//MARK:- Add marker
extension RideDetail{
    private func addPickUpMarker(){
        let pickUpLocationCustomMarkerView = CustomMarker.instanceFromNib() as! CustomMarker
        let pickUpAddress = self.rideDetail.pickUpLocation ?? "Pick-up address"
        pickUpLocationCustomMarkerView.setData(address: pickUpAddress, pin: .pickUp)
        let frame = CGRect(x: 0, y: 0, width: 120, height: 40)
        pickUpLocationCustomMarkerView.frame = frame
        self.pickUpLocationMarker.map = nil
        self.pickUpLocationMarker = GMSMarker(position: self.pickUpLocation ?? CLLocationCoordinate2D())
        self.pickUpLocationMarker.iconView = pickUpLocationCustomMarkerView
        self.pickUpLocationMarker.tracksViewChanges = true
        self.pickUpLocationMarker.map = self.mapView
    }
    private func addDropOffMarker(){
        let dropOffLocationCustomMarkerView = CustomMarker.instanceFromNib() as! CustomMarker
        let dropOffAddress = self.rideDetail.dropOffLocation ?? "Drop-off address"
        dropOffLocationCustomMarkerView.setData(address: dropOffAddress, pin: .dropOff)
        let frame = CGRect(x: 0, y: 0, width: 120, height: 40)
        dropOffLocationCustomMarkerView.frame = frame
        self.dropOffLocationMarker.map = nil
        self.dropOffLocationMarker = GMSMarker(position: self.dropOffLocation ?? CLLocationCoordinate2D())
        self.dropOffLocationMarker.iconView = dropOffLocationCustomMarkerView
        self.dropOffLocationMarker.tracksViewChanges = true
        self.dropOffLocationMarker.map = self.mapView
    }
}
//MARK:- Service
extension RideDetail{
    private func reportAProblem(){
        
        Utility.main.showAlert(message: Constants.ApiMessage, title: Strings.REPORT.text, controller: self) {
            self.navigationController?.popViewController(animated: true)
        }
        //        let ride_id = self.rideDetail.rideId
        //        let user_id = self.rideDetail.riderId ?? "0" //Rider
        //        let params:[String:Any] = ["ride_id":ride_id,
        //                                   "user_id":user_id]
        //        APIManager.sharedInstance.usersAPIManager.ReportAProblem(params: params, success: { (responseObject) in
        //            Utility.main.showAlert(message: Constants.ApiMessage, title: Strings.REPORT.text, controller: self) {
        //  self.navigationController?.popViewController(animated: true)
        
        //        }) { (error) in
        //            print(error)
    }
    
    private func getRideDetail(){
        //        let Ride_Id = self.ride.ride_id
        //        let param:[String:Any] = ["ride_Id":Ride_Id]
        //        print(param)
        //        APIManager.sharedInstance.usersAPIManager.RideDetail(params: param, success: { (responseObject) in
        //            print(responseObject)
        //            let rideDetail = Mapper<RideDetailModel>().map(JSON: responseObject) ?? RideDetailModel()
        //            if (rideDetail.rideStatus ?? "") == Strings.COMPLETED.rawValue{
        //                super.presentRateRide(rideDetail: rideDetail)
        //            }
        //        }) { (error) in
        //            print(error)
        //        }
    }
}
