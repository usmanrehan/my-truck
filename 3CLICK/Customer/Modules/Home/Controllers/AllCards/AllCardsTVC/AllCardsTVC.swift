//
//  AllCardsTVC.swift
//  3CLICK
//
//  Created by Shakeel Khan on 11/1/19.
//  Copyright © 2019 Sierra-PC. All rights reserved.
//

import UIKit

class AllCardsTVC: UITableViewCell {

    @IBOutlet weak var lblCardNumber: UILabel!
    
    func setData(data:AllCardsModel){
        self.lblCardNumber.text = "#### #### #### \(data.cardNumber ?? "")"
    }
}
