//
//  AllCards.swift
//  3CLICK
//
//  Created by Shakeel Khan on 11/1/19.
//  Copyright © 2019 Sierra-PC. All rights reserved.
//

import UIKit
import ObjectMapper
import DZNEmptyDataSet

class AllCards: BaseController {

    @IBOutlet weak var tableView: UITableView!
    var arrCards = [AllCardsModel]()
    var shouldPopToHireBookings = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = Strings.ALL_CARDS.text
        self.tableView.emptyDataSetSource = self
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        super.addNewCardButtonItem()
        self.getAllCards()
    }
    
}
extension AllCards{
    private func saveActiveCardStatusInPrefrences(){
        if self.arrCards.isEmpty{
            try! Global.APP_REALM?.write() {
                let user = AppStateManager.sharedInstance.loggedInUser
             //   user?.isCardAdded = 0
                AppStateManager.sharedInstance.loggedInUser = user
            }
        }
        else{
            var flagIsAnyCardActive = false
            for card in self.arrCards{
                if card.isActive == 1{
                    flagIsAnyCardActive = true
                    break
                }
                else{
                    flagIsAnyCardActive = false
                }
            }
            if flagIsAnyCardActive{
                try! Global.APP_REALM?.write() {
                    let user = AppStateManager.sharedInstance.loggedInUser
             //       user?.isCardAdded = 1
                    AppStateManager.sharedInstance.loggedInUser = user
                }
            }
            else{
                Utility.main.showAlert(message: Strings.ALL_INACTIVE_CARDS.text, title: Strings.ALERT.text)
                try! Global.APP_REALM?.write() {
                    let user = AppStateManager.sharedInstance.loggedInUser
           //         user?.isCardAdded = 0
                    AppStateManager.sharedInstance.loggedInUser = user
                }
            }
        }
    }
}
extension AllCards: UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.arrCards.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "AllCardsTVC", for: indexPath) as! AllCardsTVC
        let data = self.arrCards[indexPath.row]
        cell.setData(data: data)
        return cell
    }
}
extension AllCards: UITableViewDelegate{
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        var cardStatus = ""
        let card = self.arrCards[indexPath.row]
        if card.isActive == 1{
            cardStatus = Strings.IN_ACTIVE.text
        }
        else{
            cardStatus = Strings.ACTIVE.text
        }
        
        let inActiveRowAction = UITableViewRowAction(style: UITableViewRowAction.Style.default, title: cardStatus, handler:{action, indexpath in
            self.updateCardStatus(card: card)
        });
        inActiveRowAction.backgroundColor = UIColor(red: 33/255, green: 193/255, blue: 221/255, alpha: 1.0);
        
        let deleteRowAction = UITableViewRowAction(style: UITableViewRowAction.Style.default, title: Strings.DELETE.text, handler:{action, indexpath in
            self.deleteCard(card: card)
        });
        
        return [inActiveRowAction,deleteRowAction];
    }
}
extension AllCards:DZNEmptyDataSetSource{
    func title(forEmptyDataSet scrollView: UIScrollView!) -> NSAttributedString! {
        let str = Strings.NO_CARDS.text
        let attrs = [NSAttributedString.Key.font: UIFont.preferredFont(forTextStyle: UIFont.TextStyle.headline)]
        return NSAttributedString(string: str, attributes: attrs)
    }
    func description(forEmptyDataSet scrollView: UIScrollView!) -> NSAttributedString! {
        let str = Strings.NO_CARDS_DESC.text
        let attrs = [NSAttributedString.Key.font: UIFont.preferredFont(forTextStyle: UIFont.TextStyle.headline)]
        return NSAttributedString(string: str, attributes: attrs)
    }
}
//MARK:- Services
extension AllCards{
    private func getAllCards(){
        self.saveActiveCardStatusInPrefrences()
//
//        APIManager.sharedInstance.usersAPIManager.GetAllCards(params: [:], success: { (responseObject) in
//            guard let cards = responseObject as? [[String:Any]] else {return}
//            self.arrCards = Mapper<AllCardsModel>().mapArray(JSONArray: cards)
//            self.tableView.reloadData()
//            self.saveActiveCardStatusInPrefrences()
//        }) { (error) in
//            print(error)
//        }
    }
    private func updateCardStatus(card:AllCardsModel){
//        let card_id = card.cardId
//        let status = card.isActive == 1 ? "false" : "true"
//        let params:[String:Any] = ["card_id":card_id,
//                                   "status":status]
//        print(params)
//        APIManager.sharedInstance.usersAPIManager.updateCardStatus(params: params, success: { (responseObject) in
//            self.getAllCards()
//        }) { (error) in
//            print(error)
//        }
    }
    private func deleteCard(card:AllCardsModel){
//        let card_id = card.cardId
//        let params:[String:Any] = ["card_id":card_id]
//        print(params)
//        APIManager.sharedInstance.usersAPIManager.deleteCardDetails(params: params, success: { (responseObject) in
//            self.getAllCards()
//        }) { (error) in
//            print(error)
//        }
    }
}
