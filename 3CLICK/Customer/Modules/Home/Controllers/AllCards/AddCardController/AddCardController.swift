//
//  AddCardController.swift
//  3CLICK
//
//  Created by Sierra-PC on 05/09/2019.
//  Copyright © 2019 Sierra-PC. All rights reserved.
//

import UIKit
//import Stripe

class AddCardController: BaseController {

    @IBOutlet weak var lblTitle: UILableDynamicFonts!
    @IBOutlet weak var viewAddedCard: UIView!
    @IBOutlet weak var lblCardNumber: UILableDynamicFonts!
//    @IBOutlet weak var creditCardView: STPPaymentCardTextField!
    @IBOutlet weak var btnRemoveCardDetails: UIButton!
    @IBOutlet weak var lblCardHolderName: UILableDynamicFonts!
    @IBOutlet weak var lblCountry: UILableDynamicFonts!
    @IBOutlet weak var imgCountry: UIImageView!
    @IBOutlet weak var btnSave: OrangeGradient!
    
    var cardToken: String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Add Card Details"
        self.setUI()
        // Do any additional setup after loading the view.
    }
 
    @IBAction func onBtnSave(_ sender: BlueGradient) {
        self.validateCardInfo()
    }
    @IBAction func onBtnRemoveCardDetails(_ sender: UIButton) {
        self.deleteCard()
    }
    
}
//MARK:- Helper Methods
extension AddCardController{
    private func setUI(){
        let user = AppStateManager.sharedInstance.loggedInUser
      //  let isCardAdded = user?.isCardAdded == 1 ? true : false
//        if !isCardAdded{
//            self.viewAddedCard.isHidden = true
//            self.lblTitle.text = "Please add your card details"
//            self.lblCardNumber.text = ""
//            self.btnSave.isHidden = false
//            self.creditCardView.clear()
//            self.creditCardView.resignFirstResponder()
//            self.creditCardView.isEnabled = true
//        }
      //  else{
            self.viewAddedCard.isHidden = false
            self.lblTitle.text = "Your card is already added"
        //    let cardNumber = AppStateManager.sharedInstance.loggedInUser.cardNumber ?? "####"
        //    self.lblCardNumber.text = "#### #### #### \(cardNumber)"
            self.btnSave.isHidden = true
//            self.creditCardView.clear()
//            self.creditCardView.isEnabled = false
       // }
//        self.creditCardView.layer.borderColor = UIColor.clear.cgColor
        self.lblCardHolderName.text = user?.firstName ?? "-"
    }
    private func getCardToken(){
//        let cardParams = STPCardParams()
//        cardParams.number   = self.creditCardView.cardNumber
//        cardParams.expMonth = self.creditCardView.expirationMonth
//        cardParams.expYear  = self.creditCardView.expirationYear
//        cardParams.cvc      = self.creditCardView.cvc
//        Utility.showLoader()
//        STPAPIClient.shared().createToken(withCard: cardParams) { token, error in
//            if let token = token {
//                self.cardToken = "\(token)"
//                self.addNewCard()
//            }
//            if let error = error {
//                Utility.hideLoader()
//                self.btnSave.shake()
//                Utility.main.showToast(message: error.localizedDescription)
//            }
//        }
    }
    private func deleteCard(){
        Utility.main.showAlert(message: Strings.DELETE_CARD.text, title: Strings.ALERT.text, YES: Strings.YES.text, NO: Strings.NO.text, controller: self) { (Yes, No) in
            if Yes != nil{
                self.deleteCardInfo()
            }
        }
    }
    private func validateCardInfo(){
//        let number = self.creditCardView.cardNumber
//        let expMonth = self.creditCardView.expirationMonth
//        let expYear = self.creditCardView.expirationYear
//        let cvc = self.creditCardView.cvc
//        if (number?.count ?? 0) != 16{
//            self.btnSave.shake()
//            Utility.main.showToast(message: Strings.INVALID_CARD_NUMBER.text)
//            return
//        }
        self.getCardToken()
    }
}
//MARK:- Services
extension AddCardController{
    private func addNewCard(){
        Utility.main.showAlert(message: Strings.NEW_CARD_ADDED.text, title: Strings.Confirmation.text, controller: self, usingCompletionHandler: {
                       self.navigationController?.popViewController(animated: true)
                   })
//        let card_number = String(self.creditCardView.cardNumber?.suffix(4) ?? "####")
//        let token = self.cardToken ?? ""
//        let params:[String:Any] = ["card_number":card_number,"token":token]
//        APIManager.sharedInstance.usersAPIManager.AddCardDetails(params: params, success: { (responseObject) in
//            print(responseObject)
//            Utility.main.showAlert(message: Strings.NEW_CARD_ADDED.text, title: Strings.Confirmation.text, controller: self, usingCompletionHandler: {
//                self.navigationController?.popViewController(animated: true)
//            })
//        }) { (error) in
//            print(error)
//        }
    }
    
    private func updateCardInfo(){
        Utility.main.showAlert(message: Strings.CARD_ADDED.text, title: Strings.ALERT.text, controller: self, usingCompletionHandler: {
            self.navigationController?.popViewController(animated: true)
        })
//        let customer_id = self.cardToken ?? ""
//        let card_num = String(self.creditCardView.cardNumber?.suffix(4) ?? "####")
//        let params:[String:Any] = ["customer_id":customer_id,"card_num":card_num]
//        APIManager.sharedInstance.usersAPIManager.AddCard(params: params, success: { (responseObject) in
//            try! Global.APP_REALM?.write() {
//                let user = AppStateManager.sharedInstance.loggedInUser
//                user?.cardNumber = card_num
//                user?.isCardAdded = 1
//                AppStateManager.sharedInstance.loggedInUser = user
//            }
//            Utility.main.showAlert(message: Strings.CARD_ADDED.text, title: Strings.ALERT.text, controller: self, usingCompletionHandler: {
//                self.navigationController?.popViewController(animated: true)
//            })
//            self.setUI()
//        }) { (error) in
//            print(error)
//        }
    }
    private func deleteCardInfo(){
        Utility.main.showAlert(message: Strings.CARD_DELETED.text, title: Strings.ALERT.text)
        self.setUI()
//
//        let customer_id = "0"
//        let card_num = "0"
//        let params:[String:Any] = ["customer_id":customer_id,"card_num":card_num]
//        APIManager.sharedInstance.usersAPIManager.RemoveCard(params: params, success: { (responseObject) in
//            try! Global.APP_REALM?.write() {
//                let user = AppStateManager.sharedInstance.loggedInUser
//                user?.cardNumber = nil
//                user?.cardId = nil
//                user?.isCardAdded = 0
//                AppStateManager.sharedInstance.loggedInUser = user
//            }
//            Utility.main.showAlert(message: Strings.CARD_DELETED.text, title: Strings.ALERT.text)
//            self.setUI()
//        }) { (error) in
//            print(error)
//        }
    }
}
