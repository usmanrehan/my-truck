//
//  CancelBooking.swift
//  3CLICK
//
//  Created by Mac Book on 09/01/2020.
//  Copyright © 2020 Sierra-PC. All rights reserved.
//

import UIKit

class CancelBooking: BaseController {

    @IBOutlet weak var tvReason: UITextView!
    
    var rideDetail: RideDetailModel?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    @IBAction func onBtnClose(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    @IBAction func onBtnSubmit(_ sender: UIButton) {
        self.validate(sender)
    }
}
//MARK:- Helper Methods
extension CancelBooking{
    private func validate(_ sender:UIButton){
        self.tvReason.text = (self.tvReason.text ?? "").trimmingCharacters(in: .whitespacesAndNewlines)
        if !Validation.validateStringLength(self.tvReason.text ?? ""){
            Utility.main.showAlert(message: Strings.CANCEL_REASON.text, title: Strings.ERROR.text)
            sender.shake()
            return
        }
        self.cancelBooking()
    }
}
//MARK:- Services
extension CancelBooking{
    private func cancelBooking(){
        let id = rideDetail?.id ?? ""
        let cancelledReason = self.tvReason.text ?? ""
        let param:[String:Any] = ["cancelledReason":cancelledReason]
        APIManager.sharedInstance.usersAPIManager.CancelBooking(params: param, success: { (responseObject) in
            self.dismiss(animated: true, completion: nil)
        }, failure: { (error) in
            print(error)
        }, id: id)
    }
}
