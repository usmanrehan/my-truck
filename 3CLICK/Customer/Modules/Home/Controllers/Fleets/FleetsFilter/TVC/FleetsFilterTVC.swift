//
//  FleetsFilterTVC.swift
//  3CLICK
//
//  Created by Sierra-PC on 28/05/2019.
//  Copyright © 2019 Sierra-PC. All rights reserved.
//

import UIKit

class FleetsFilterTVC: UITableViewCell {

    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var imgTick: UIImageView!
    
    func setData(_ data:FleetsFilterData){
        self.lblTitle.text = data.title
        if data.isSelected{
            self.imgTick.isHidden = false
        }
        else{
            self.imgTick.isHidden = true
        }
    }
    
}
