//
//  FleetsFilter.swift
//  3CLICK
//
//  Created by Sierra-PC on 28/05/2019.
//  Copyright © 2019 Sierra-PC. All rights reserved.
//

import UIKit

struct FleetsFilterData {
    var title:String
    var isSelected:Bool
}

class FleetsFilter: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    
    var arrFleetsFilterData = [FleetsFilterData]()
    var riderType = RiderType.both
    var selectedRide: ((RiderType)->Void)?
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.setUI()
    }

    @IBAction func onBtnOk(_ sender: BlueGradient) {
        self.dismiss(animated: true) {
            self.selectedRide?(self.riderType)
        }
    }
    @IBAction func onBtnCancel(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
}
//MARK:- Helper Methods
extension FleetsFilter{
    private func setUI(){
        switch self.riderType{
        case .both:
            self.arrFleetsFilterData = [FleetsFilterData(title: "Fleet Company", isSelected: false),
                                        FleetsFilterData(title: "Individual", isSelected: false),
                                        FleetsFilterData(title: "Both", isSelected: true)]
        case .individual:
            self.arrFleetsFilterData = [FleetsFilterData(title: "Fleet Company", isSelected: false),
                                        FleetsFilterData(title: "Individual", isSelected: true),
                                        FleetsFilterData(title: "Both", isSelected: false)]
        case .company:
            self.arrFleetsFilterData = [FleetsFilterData(title: "Fleet Company", isSelected: true),
                                        FleetsFilterData(title: "Individual", isSelected: false),
                                        FleetsFilterData(title: "Both", isSelected: false)]
        }
        self.tableView.reloadData()
    }
}
//MARK:- UITableViewDataSource
extension FleetsFilter:UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.arrFleetsFilterData.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "FleetsFilterTVC", for: indexPath) as! FleetsFilterTVC
        let data = self.arrFleetsFilterData[indexPath.row]
        cell.setData(data)
        return cell
    }
}
//MARK:- UITableViewDelegate
extension FleetsFilter:UITableViewDelegate{
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        switch indexPath.row{
        case 0:
            self.riderType = .company
        case 1:
            self.riderType = .individual
        case 2:
            self.riderType = .both
        default:
            break
        }
        if self.arrFleetsFilterData[indexPath.row].isSelected{return}
        for i in 0..<self.arrFleetsFilterData.count{
            self.arrFleetsFilterData[i].isSelected = false
        }
        self.arrFleetsFilterData[indexPath.row].isSelected = true
        self.tableView.reloadSections([0], with: .fade)
    }
}
