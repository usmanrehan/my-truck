//
//  SideMenu.swift
//  3CLICK
//
//  Created by Sierra-PC on 21/05/2019.
//  Copyright © 2019 Sierra-PC. All rights reserved.
//

import UIKit

class SideMenu: UIViewController {

    @IBOutlet weak var imgProfile: RoundedImage!
    @IBOutlet weak var lblCustomerName: UILabel!
    @IBOutlet weak var ratingView: CosmosView!
    @IBOutlet weak var tableView: UITableView!
    
    var arrSideMenuData = [SideMenuData(icon: UIImage(named: "aboutUs"),   option: "About Us", notificationCount: nil),
                           SideMenuData(icon: UIImage(named: "Bookings"),  option: "Hire Now", notificationCount: nil),
                           SideMenuData(icon: UIImage(named: "Transactions"),  option: "Transactions", notificationCount: nil),
                           SideMenuData(icon: UIImage(named: "settings"),  option: "Settings", notificationCount: nil),
                           SideMenuData(icon: UIImage(named: "bell"),      option: "Notifications", notificationCount: 0),
                           SideMenuData(icon: UIImage(named: "truckIcon"), option: "Trips", notificationCount: nil),
                           SideMenuData(icon: UIImage(named: "help"),      option: "Help", notificationCount: nil),
                           SideMenuData(icon: UIImage(named: "logout"),    option: "Logout", notificationCount: nil)]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setData()
        // Do any additional setup after loading the view.
    }

}
extension SideMenu{
    private func setData(){
        let user = AppStateManager.sharedInstance.loggedInUser
        self.lblCustomerName.text = user?.firstName
        if let imgURL = URL(string: user?.profilePic ?? ""){
            self.imgProfile.sd_setImage(with: imgURL, placeholderImage: UIImage(named: "profilePlaceholder"), options: .highPriority, completed: nil)
        }
    }
}
extension SideMenu: UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.arrSideMenuData.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "SideMenuTVC", for: indexPath) as! SideMenuTVC
        cell.setData(data: self.arrSideMenuData[indexPath.row])
        return cell
    }
}
extension SideMenu: UITableViewDelegate{
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.changeContentWith(index: indexPath.row)
    }
}
extension SideMenu{
    private func changeContentWith(index:Int){
        if index == self.arrSideMenuData.count - 1{
            AppStateManager.sharedInstance.logoutUser()
            return
        }
        self.sideMenuController?.hideLeftViewAnimated()
        switch index {
        case 0:
            self.changeContentToAboutUs()
        case 1:
            self.changeContentToHome()
        case 2:
            self.changeContentToTransactions()
        case 3:
            self.changeContentToSettings()
        case 4:
            self.changeContentToNotifications()
        case 5:
            self.changeContentToYourRides()
        case 6:
            self.changeContentToHelp()
        default:
            break
        }
    }
    private func changeContentToHome(){
        let storyboard = AppStoryboard.Home.instance
        let controller = storyboard.instantiateViewController(withIdentifier: "Home")
        let navController = BaseNavigationController(rootViewController: controller)
        self.sideMenuController?.rootViewController = navController
    }
    private func changeContentToTransactions(){
        let storyboard = AppStoryboard.Home.instance
        let controller = storyboard.instantiateViewController(withIdentifier: "Transactions")
        let navController = BaseNavigationController(rootViewController: controller)
        self.sideMenuController?.rootViewController = navController
    }
    private func changeContentToYourRides(){
        let storyboard = AppStoryboard.Home.instance
        let controller = storyboard.instantiateViewController(withIdentifier: "YourRides")
        let navController = BaseNavigationController(rootViewController: controller)
        self.sideMenuController?.rootViewController = navController
    }
    private func changeAddCreditCard(){
        let storyboard = AppStoryboard.Home.instance
        let controller = storyboard.instantiateViewController(withIdentifier: "AddCardController")
        let navController = BaseNavigationController(rootViewController: controller)
        self.sideMenuController?.rootViewController = navController
    }
    private func changeContentToAboutUs(){
        let storyboard = AppStoryboard.Home.instance
        let controller = storyboard.instantiateViewController(withIdentifier: "CMS") as! CMS
        controller.cmsType = .aboutUs
        let navController = BaseNavigationController(rootViewController: controller)
        self.sideMenuController?.rootViewController = navController
    }
    private func changeContentToNotifications(){
        let storyboard = AppStoryboard.Home.instance
        let controller = storyboard.instantiateViewController(withIdentifier: "Notifications")
        let navController = BaseNavigationController(rootViewController: controller)
        self.sideMenuController?.rootViewController = navController
    }
    private func changeContentToHelp(){
        let storyboard = AppStoryboard.Home.instance
        let controller = storyboard.instantiateViewController(withIdentifier: "CMS") as! CMS
        controller.cmsType = .help
        let navController = BaseNavigationController(rootViewController: controller)
        self.sideMenuController?.rootViewController = navController
    }
    private func changeContentToSettings(){
        let storyboard = AppStoryboard.Home.instance
        let controller = storyboard.instantiateViewController(withIdentifier: "Settings")
        let navController = BaseNavigationController(rootViewController: controller)
        self.sideMenuController?.rootViewController = navController
    }
}
