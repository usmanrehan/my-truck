//
//  SideMenuTVC.swift
//  3CLICK
//
//  Created by Sierra-PC on 21/05/2019.
//  Copyright © 2019 Sierra-PC. All rights reserved.
//

import UIKit

struct SideMenuData {
    var icon:UIImage?
    var option:String?
    var notificationCount:Int?
}

class SideMenuTVC: UITableViewCell {
    
    @IBOutlet weak var imgIcon: UIImageView!
    @IBOutlet weak var notificationView: RoundedView!
    @IBOutlet weak var lblNotificationCount: UILabel!
    @IBOutlet weak var lblOption: UILabel!
    
    func setData(data:SideMenuData){
        self.imgIcon.image = data.icon
        self.lblOption.text = data.option
        if let notificationCount = data.notificationCount, notificationCount != 0{
            self.notificationView.isHidden = false
            self.lblNotificationCount.text = "\(notificationCount)"
        }
        else{
            self.notificationView.isHidden = true
        }
    }
}
