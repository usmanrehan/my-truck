//
//  BookingsModel.swift
//
//  Created by Hamza Hasan on 04/01/2020
//  Copyright (c) . All rights reserved.
//


import ObjectMapper
import RealmSwift
import ObjectMapper_Realm

public class Bookings: Object, Mappable {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let deliveryDate = "deliveryDate"
    static let id = "_id"
    static let bookingStatus = "bookingStatus"
    static let dropOffLocation = "dropOffLocation"
    static let pickUpLocation = "pickUpLocation"
    static let orderNumber = "orderNumber"
    static let vendor = "vendor"
    static let pickUpCity = "pickUpCity"
    static let amount = "customerPayable"
    static let customer = "customer"
    static let rating = "rating"
  }

  // MARK: Properties
  @objc dynamic var deliveryDate: String? = ""
  @objc dynamic var id: String? = ""
  @objc dynamic var bookingStatus: String? = ""
  @objc dynamic var dropOffLocation: String? = ""
  @objc dynamic var pickUpLocation: String? = ""
  @objc dynamic var orderNumber: String? = ""
  @objc dynamic var vendor: Vendor?
  @objc dynamic var pickUpCity: PickUpCity?
  @objc dynamic var amount = 0
  @objc dynamic var customer: Customer?
  @objc dynamic var rating = -1

  // MARK: ObjectMapper Initializers
  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.

    required convenience public init?(map : Map){
    self.init()
  }

    override public class func primaryKey() -> String? {
    return "id"
  }

  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.
  public func mapping(map: Map) {
    deliveryDate <- map[SerializationKeys.deliveryDate]
    id <- map[SerializationKeys.id]
    bookingStatus <- map[SerializationKeys.bookingStatus]
    dropOffLocation <- map[SerializationKeys.dropOffLocation]
    pickUpLocation <- map[SerializationKeys.pickUpLocation]
    orderNumber <- map[SerializationKeys.orderNumber]
    vendor <- map[SerializationKeys.vendor]
    pickUpCity <- map[SerializationKeys.pickUpCity]
    amount <- map[SerializationKeys.amount]
    customer <- map[SerializationKeys.customer]
    rating <- map[SerializationKeys.rating]
  }


}
