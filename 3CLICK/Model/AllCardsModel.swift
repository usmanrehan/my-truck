//
//  AllCardsModel.swift
//  3CLICK
//
//  Created by Shakeel Khan on 11/1/19.
//  Copyright © 2019 Sierra-PC. All rights reserved.
//

import Foundation
import ObjectMapper
import RealmSwift

public class AllCardsModel: Object, Mappable {
    
    // MARK: Declaration for string constants to be used to decode and also serialize.
    private struct SerializationKeys {
        static let cardId = "card_id"
        static let cardNumber = "card_number"
        static let isActive = "is_active"
    }
    // MARK: Properties
    @objc dynamic var cardId = 0
    @objc dynamic var cardNumber: String? = ""
    @objc dynamic var isActive = 0
    // MARK: ObjectMapper Initializers
    /// Map a JSON object to this class using ObjectMapper.
    ///
    /// - parameter map: A mapping from ObjectMapper.
    
    required convenience public init?(map : Map){
        self.init()
    }
    
    override public class func primaryKey() -> String? {
        return "cardId"
    }
    
    /// Map a JSON object to this class using ObjectMapper.
    ///
    /// - parameter map: A mapping from ObjectMapper.
    public func mapping(map: Map) {
        cardId <- map[SerializationKeys.cardId]
        cardNumber <- map[SerializationKeys.cardNumber]
        isActive <- map[SerializationKeys.isActive]
    }
}
