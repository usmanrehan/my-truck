//
//  Data.swift
//
//  Created by Hamza Hasan on 04/12/2019
//  Copyright (c) . All rights reserved.
//


import ObjectMapper
import RealmSwift
import ObjectMapper_Realm

public class CityModel: Object, Mappable {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let name = "name"
    static let location = "location"
    static let id = "_id"
    static let isDeleted = "isDeleted"
    static let createdAt = "createdAt"
    static let v = "__v"
    static let updatedAt = "updatedAt"
    static let active = "active"
  }

  // MARK: Properties
  @objc dynamic var name: String? = ""
//  dynamic var location: Location?
  @objc dynamic var id: String? = ""
  @objc dynamic var isDeleted = false
  @objc dynamic var createdAt: String? = ""
  @objc dynamic var v = 0
  @objc dynamic var updatedAt: String? = ""
  @objc dynamic var active = false

  // MARK: ObjectMapper Initializers
  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.

    required convenience public init?(map : Map){
    self.init()
  }

    override public class func primaryKey() -> String? {
    return "id"
  }

  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.
  public func mapping(map: Map) {
    name <- map[SerializationKeys.name]
 //   location <- map[SerializationKeys.location]
    id <- map[SerializationKeys.id]
    isDeleted <- map[SerializationKeys.isDeleted]
    createdAt <- map[SerializationKeys.createdAt]
    v <- map[SerializationKeys.v]
    updatedAt <- map[SerializationKeys.updatedAt]
    active <- map[SerializationKeys.active]
  }


}
