//
//  StateModel.swift
//
//  Created by Hamza Hasan on 04/12/2019
//  Copyright (c) . All rights reserved.
//


import ObjectMapper
import RealmSwift
import ObjectMapper_Realm

public class StateModel: Object, Mappable {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let success = "success"
    static let data = "data"
    static let page = "page"
    static let count = "count"
  }

  // MARK: Properties
  @objc dynamic var success = false
  var data = List<Data>()
  @objc dynamic var page = 0
  @objc dynamic var count = 0

  // MARK: ObjectMapper Initializers
  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.

    required convenience public init?(map : Map){
    self.init()
  }

    override public class func primaryKey() -> String? {
    return "id"
  }

  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.
  public func mapping(map: Map) {
    success <- map[SerializationKeys.success]
    data <- (map[SerializationKeys.data], ListTransform<Data>())
    page <- map[SerializationKeys.page]
    count <- map[SerializationKeys.count]
  }


}
