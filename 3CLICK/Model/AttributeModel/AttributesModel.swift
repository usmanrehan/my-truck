//
//  AttributesModel.swift
//
//  Created by Hamza Hasan on 24/12/2019
//  Copyright (c) . All rights reserved.
//


import ObjectMapper
import RealmSwift
import ObjectMapper_Realm

public class AttributesModel: Object, Mappable {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let comidity = "comidity"
    static let vehileTypes = "vehileTypes"
    static let weight = "weightRange"
  }

  // MARK: Properties
  var comidity = List<AttributeModel>()
  var vehileTypes = List<AttributeModel>()
  var weight = List<AttributeModel>()
  @objc dynamic var id: String? = ""

  // MARK: ObjectMapper Initializers
  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.

    required convenience public init?(map : Map){
    self.init()
  }

    override public class func primaryKey() -> String? {
    return "id"
  }

  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.
  public func mapping(map: Map) {
    comidity <- (map[SerializationKeys.comidity], ListTransform<AttributeModel>())
    vehileTypes <- (map[SerializationKeys.vehileTypes], ListTransform<AttributeModel>())
    weight <- (map[SerializationKeys.weight], ListTransform<AttributeModel>())
  }


}
