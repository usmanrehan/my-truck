//
//  FleetsModel.swift
//
//  Created by Hamza Hasan on 01/01/2020
//  Copyright (c) . All rights reserved.
//


import ObjectMapper
import RealmSwift
import ObjectMapper_Realm

public class FleetsModel: Object, Mappable {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let status = "status"
    static let id = "_id"
    static let profilePic = "profilePic"
    static let email = "email"
    static let lastName = "lastName"
    static let rating = "rating"
    static let firstName = "firstName"
    static let role = "role"
    static let amount = "amount"
    static let aboutCompany = "aboutCompany"
    static let companyName = "companyName"
    static let rateId = "rateId"
  }

  // MARK: Properties
  @objc dynamic var status: String? = ""
  @objc dynamic var id: String? = ""
  @objc dynamic var profilePic: String? = ""
  @objc dynamic var email: String? = ""
  @objc dynamic var lastName: String? = ""
  @objc dynamic var rating = 0.0
  @objc dynamic var firstName: String? = ""
  @objc dynamic var role: Role?
  @objc dynamic var amount: String? = ""
  @objc dynamic var aboutCompany: String? = ""
  @objc dynamic var companyName: String? = ""
  @objc dynamic var rateId: String? = ""

  // MARK: ObjectMapper Initializers
  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.

    required convenience public init?(map : Map){
    self.init()
  }

    override public class func primaryKey() -> String? {
    return "id"
  }

  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.
  public func mapping(map: Map) {
    status <- map[SerializationKeys.status]
    id <- map[SerializationKeys.id]
    profilePic <- map[SerializationKeys.profilePic]
    email <- map[SerializationKeys.email]
    lastName <- map[SerializationKeys.lastName]
    rating <- map[SerializationKeys.rating]
    firstName <- map[SerializationKeys.firstName]
    role <- map[SerializationKeys.role]
    amount <- map[SerializationKeys.amount]
    aboutCompany <- map[SerializationKeys.aboutCompany]
    companyName <- map[SerializationKeys.companyName]
    rateId <- map[SerializationKeys.rateId]
  }


}
